def plus(n1, n2):
    """Додавання двох чисел"""
    return n1 + n2


def minus(n1, n2):
    """Віднімання двох чисел"""
    return n1 - n2


def multiply(n1, n2):
    """Множення двох чисел"""
    return n1 * n2


def divide(n1, n2):
    """Ділення двох чисел"""
    return n1 / n2


def power(n1, n2):
    """Зведення у ступінь"""
    if n1 == 0 and n2 < 0:
        raise ZeroNegativePowerError("Не можна нуль зводити у від'ємну ступінь!")
    return n1**n2


class ZeroNegativePowerError(Exception):
    pass


if __name__ == "__main__":
    while True:
        print(
            "1. Додавання\n2. Віднімання\n3. Множення\n4. Ділення\n"
            "5. Зведення у ступінь\n\n0. Вихід\n\n"
        )

        try:
            n = int(input("Зробіть вибір: "))

            if not 0 <= n <= 5:
                raise ValueError("Error: Некоректний вибір у меню")

            if n == 0:
                break

            n1 = float(input("Перше число: "))
            n2 = float(input("Друге число: "))
        except ValueError as ex:
            print(f"{ex.__class__}: ", ex)
            continue

        result = None

        try:
            match n:
                case 1:
                    result = plus(n1, n2)
                case 2:
                    result = minus(n1, n2)
                case 3:
                    result = multiply(n1, n2)
                case 4:
                    result = divide(n1, n2)
                case 5:
                    result = power(n1, n2)
        except ZeroDivisionError as ex:
            print("Error: Не можна ділити на нуль!")
        except TypeError as ex:
            print("Error: Некоректний тип даних!")
        except Exception as ex:
            print(f"{ex.__class__}: ", ex)
        else:
            print(f"Результат: {result}")

        input("\n\nPress Enter\n ")
