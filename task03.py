class Employee:
    """Клас Співробітник"""

    def __init__(self, name, surname, department, start_year):
        self.name = name
        self.surname = surname
        self.department = department
        try:
            self.start_year = int(start_year)
            if not 1950 <= self.start_year <= 2024:
                raise ValueError
        except ValueError:
            raise ValueError("Error: Введений некоректний рік початку роботи!")
        except Exception:
            raise

        print("Створено: ", self)

    def __str__(self):
        return f"{self.name} {self.surname} - {self.department} - {self.start_year} р."

    @staticmethod
    def report_employees(employees, year):
        """Відображає список співробітників, які почали працювати із вказаного року"""
        print(f"ЗВІТ ПО ПРАЦІВНИКАМ, ЩО ПОЧАЛИ ПРАЦЮІВАТИ З {year} РОКУ\n")
        print("-" * 55)
        n = 0

        for employee in employees:
            if employee.start_year == year:
                print(employee)
                n += 1

        print("-" * 55)
        print(f"Всього рядків: {n}")


employees = [
    Employee("Іван", "Іванов", "транспортний", 2022),
    Employee("Петро", "Петров", "бухгалтерія", 2020),
    Employee("Сидір", "Сидоров", "склад", 2024),
]

if __name__ == "__main__":
    while True:
        print("\n\nВВЕДЕННЯ ДАНИХ ПО ПРАЦІВНИКАМ\n\n")
        name = input("Ім'я (або 0 для завершення): ")
        if name == "0":
            break

        surname = input("Прізвище: ")
        department = input("Відділ: ")
        start_year = input("Рік початку роботи: ")

        try:
            employee = Employee(name, surname, department, start_year)
            employees.append(employee)
        except Exception as ex:
            print(f"{ex.__class__}: {ex}")

    Employee.report_employees(employees, 2024)
