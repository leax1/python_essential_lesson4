class SpecificError(Exception):
    pass


def my_func(n: int):
    """My unique function"""
    if n == 0:
        raise SpecificError("Error: Don't use 0 as argument of my_func!")

    return n * 2


if __name__ == "__main__":
    try:
        print(my_func(5))
        print(my_func(0))
        print(my_func(-1))
    except SpecificError as e:
        print(f"{e.__class__}: {e}")
