class TrainerNotFound(Exception):
    pass


def print_dict(dictionary, header: str):
    """Друкує вказаний словник"""
    print(f"\n{header}\n")
    for key, value in dictionary.items():
        print(f"{key}: {value}")

    print()


if __name__ == "__main__":
    sports = {1: "шахи", 2: "теніс", 3: "бобслей", 4: "стрибки у ширину"}
    trainers = {
        "Іванов": "шахи, теніс",
        "Петров": "бобслей",
        "Сидоров": "стрибки у ширину",
    }
    schedules = {1: "10:00-12:00", 2: "14:00-16:00", 3: "18:00-20:00"}
    costs = {"mini": 1200, "middle": 1400, "maxi": 1800}

    while True:
        print(
            "МЕНЮ\n\n1. Перелік видів спорту\n2. Команда тренерів\n3. Розклад тренувань"
            "\n4. Вартість тренування\n5. Пошук тренера\n\n0. Вихід\n"
        )

        n = int(input("Зробіть вибір: "))
        if not 0 <= n <= 5:
            print("Error: Невірний вибір!")
            continue

        if n == 0:
            break

        match n:
            case 1:
                print_dict(sports, "Види спорту")
            case 2:
                print_dict(trainers, "Команда тренерів")
            case 3:
                print_dict(schedules, "Розклад тренувань")
            case 4:
                print_dict(costs, "Вартість тренувань")
            case 5:
                trainer = input("Введіть прізвище тренера: ")

                try:
                    print(f"Тренер {trainer} тренує по: {trainers[trainer]}")
                except KeyError as e:
                    raise TrainerNotFound(
                        f"Error: Немає тренера з прізвищем {trainer}!"
                    )

        input("\nPress Enter to continue...")
